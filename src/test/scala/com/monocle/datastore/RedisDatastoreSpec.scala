package com.monocle.datastore

import java.util.UUID
import java.util.concurrent.TimeUnit

import cats.data.EitherT
import com.monocle.datastore.api.common.CommonResponse.RepoResponseF
import com.monocle.datastore.util.RedisFormats._
import com.monocle.datastore.util.{RedisFixture, RedisFormats, UserProfile}
import org.scalatest.{FlatSpec, Matchers}

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration.Duration
import scala.concurrent.{Await, Future}

class RedisDatastoreSpec extends FlatSpec with Matchers {

  private lazy val redisConfig  = RedisFixture.createRedisConfiguration
  private lazy val redisDB      = RedisFixture.redisDB(redisConfig)

  private def newUser(email: String, firstName: String, lastName: String): UserProfile = UserProfile(UUID.randomUUID().toString, System.currentTimeMillis, email, firstName, lastName)

  "User profile" should "be set" in {
    val userProfile = newUser("testUser@email.com", "Test", "User")

    val responseE = exec(redisDB.set(userProfile.id, userProfile))

    assert(responseE.isRight)

    val response = responseE.getOrElse(false)

    assert(response)

    val optProfile0E = exec(redisDB.get[UserProfile](userProfile.id))

    assert(optProfile0E.isRight)

    val optProfile0 = optProfile0E.getOrElse(None)

    assert(optProfile0.isDefined)

    val foundProfile = optProfile0.get

    assert(foundProfile.id == userProfile.id)

    val deleteResultE = exec(redisDB.delete(userProfile.id))

    assert(deleteResultE.isRight)

    val deleteResult = deleteResultE.getOrElse(None)

    assert(deleteResult.isDefined)

    val optProfile1E = exec(redisDB.get[UserProfile](userProfile.id))

    assert(optProfile1E.isRight)

    val optProfile1 = optProfile1E.getOrElse(None)

    assert(optProfile1.isEmpty)
  }

  "Redis interpreter" should "interpret as expected" in {
    import cats.implicits._
    import com.monocle.datastore.api.redis.RedisInterpreter._

    val user = newUser("test@email.com", "FirstName", "LastName")

    def program: RedisIOF[Option[UserProfile]] = {
      val jsonFormatter = RedisFormats.userProfileJsonFormat

      for {
        _             <- set(user.id, user, jsonFormatter)
        readResponse  <- get(user.id, jsonFormatter)
      } yield readResponse
    }

    // Executing our `program`
    val resultFuture: RepoResponseF[Option[UserProfile]] = program.foldMap(redisInterpreter(redisDB))

    val resultE = exec(resultFuture)

    assert(resultE.isRight)

    val resultF = resultE.getOrElse(None)

    assert(resultF.isDefined)

    val foundUser = resultF.get

    assert(foundUser.id == user.id)
  }

  private def exec[A, B](eitherT: EitherT[Future, A, B]): Either[A, B] = Await.result(eitherT.value, Duration(5, TimeUnit.SECONDS))
}

package com.monocle.datastore

import java.util.UUID
import java.util.concurrent.TimeUnit

import cats.data.EitherT
import com.monocle.datastore.api.common.CommonResponse.RepoResponseF
import com.monocle.datastore.api.sql.SqlCommon.SqlResponse
import com.monocle.datastore.errors.RepoError
import com.monocle.datastore.util.{SqlFixture, UserProfile, UserProfileRepo}
import org.scalatest.{FlatSpec, Matchers}
import slick.jdbc.MySQLProfile.api._

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration.Duration
import scala.concurrent.{Await, Future}

class SqlDatastoreSpec extends FlatSpec with Matchers {

  implicit private lazy val db = SqlFixture.sqlDB

  private def newUser(email: String, firstName: String, lastName: String): UserProfile = UserProfile(UUID.randomUUID().toString, System.currentTimeMillis, email, firstName, lastName)


  "User profile" should "save" in {

    exec(db.createTables)

    val userProfile = newUser("testUser@email.com", "Test", "User")

    exec(UserProfileRepo.saveUserProfile(userProfile))

    val foundUser: SqlResponse[Option[UserProfile]] = exec(db.read[Option[UserProfile]](_.userTable.filter(_.id === userProfile.id).result.headOption))

    assert(foundUser.isRight)

    val userOpt = foundUser.right.get

    assert(userOpt.isDefined)

    val userF0 = exec(UserProfileRepo.findById(userProfile.id))

    assert(userF0.isRight && userF0.right.get.isDefined)
    assert(userF0.right.get.nonEmpty)

    val userF1 = exec(UserProfileRepo.findById(userProfile.id))

    assert(userF1.isRight)

    val userOpt1 = userF1.right.get

    assert(userOpt1.isDefined)
  }

  "Sql interpreter" should "interpret as expected" in {
    import cats.implicits._
    import com.monocle.datastore.api.sql.MySqlInterpreter._

    val user = newUser("test@email.com", "firstName", "lastName")

    def program: MySqlIOF[Option[UserProfile]] = {
      for {
        _             <- write(db)(tables => tables.userTable += user)
        readResponse  <- read(db)(tables => tables.userTable.filter(_.id === user.id).result.headOption )
      } yield readResponse
    }

    // Executing our `program`
    val resultF: RepoResponseF[Option[UserProfile]] = program.foldMap(mySqlInterpreter(db))

    val optUserE: Either[RepoError, Option[UserProfile]] = exec(resultF)

    assert(optUserE.isRight)

    val optUser = optUserE.getOrElse(None)

    assert(optUser.isDefined)

    val foundUser = optUser.get

    assert(foundUser.id == user.id)
  }

  private def exec[A](f: Future[A]): A = Await.result(f, Duration(5, TimeUnit.SECONDS))
  private def exec[A, B](eitherT: EitherT[Future, A, B]): Either[A, B] = Await.result(eitherT.value, Duration(5, TimeUnit.SECONDS))
}

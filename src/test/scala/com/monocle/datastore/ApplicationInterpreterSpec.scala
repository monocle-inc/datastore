package com.monocle.datastore

import java.util.UUID
import java.util.concurrent.TimeUnit

import cats.data.EitherT
import cats.free.Free
import com.monocle.datastore.api.common.CommonInterpreter
import com.monocle.datastore.api.common.CommonInterpreter.DataStoreApplication
import com.monocle.datastore.api.common.CommonResponse.RepoResponseF
import com.monocle.datastore.api.mongo.MongoInjectedInteractions
import com.monocle.datastore.api.redis.RedisInjectedInteractions
import com.monocle.datastore.api.sql.MySqlInjectedInteractions
import com.monocle.datastore.util._
import org.scalatest.{FlatSpec, Matchers}
import reactivemongo.bson.BSONDocument

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration.Duration
import scala.concurrent.{Await, Future}

class ApplicationInterpreterSpec extends FlatSpec with Matchers {

  private lazy val mongoConfig  = MongoFixture.createMongoConfiguration
  private lazy val mongoRepo    = MongoFixture.mongoCollection0(mongoConfig)

  implicit lazy val sqlRepo     = SqlFixture.sqlDB

  private lazy val redisConfig  = RedisFixture.createRedisConfiguration
  private lazy val redisRepo    = RedisFixture.redisDB(redisConfig)

  private def newUser(email: String, firstName: String, lastName: String): UserProfile = UserProfile(UUID.randomUUID().toString, System.currentTimeMillis, email, firstName, lastName)

  "Application interpreter" should "interpret all as expected" in {
    import CommonInterpreter._
    import cats.implicits._

    val user = newUser("testUser@email.com", "FirstName", "LastName")
    val programResultF: RepoResponseF[Option[UserProfile]] = program(user).foldMap(interpreter(redisRepo, mongoRepo, sqlRepo))

    val result = exec(programResultF)

    assert(result.isRight)

    val optUser = result.getOrElse(None)

    assert(optUser.isDefined)

    val foundUser = optUser.get

    assert(foundUser.id == user.id)
  }

  private def program(user: UserProfile)(implicit
                                         R: RedisInjectedInteractions[DataStoreApplication],
                                         M: MongoInjectedInteractions[DataStoreApplication],
                                         S: MySqlInjectedInteractions[DataStoreApplication]): Free[DataStoreApplication, Option[UserProfile]] = {
    import M._
    import R._
    import S._
    import slick.jdbc.MySQLProfile.api._

    val bsonHandler   = BSONTestHandler.userProfileBSONHandler
    val findQuery     = BSONDocument("id" -> user.id)
    val jsonFormatter = RedisFormats.userProfileJsonFormat

    /*
     *  1. Set user in Redis
     *  2. Find user in Redis and persist to Mongo
     *  3. Read from Mongo and persist user to MySql
     *  4. Read from MySql and return user
     */
    for {
      _         <- writeToRedis(user.id, user, jsonFormatter)
      fRedis    <- readFromRedis[UserProfile](user.id, jsonFormatter)
      _         <- insertInMongo(fRedis.get, bsonHandler)
      fMongo    <- readOneFromMongo[UserProfile](findQuery, bsonHandler)
      _         <- writeToMySql(sqlRepo)(_.userTable += fMongo.get)
      response  <- readFromMySql(sqlRepo)(tables => tables.userTable.filter(_.id === user.id).result.headOption)
    } yield response
  }

  private def exec[A, B](eitherT: EitherT[Future, A, B]): Either[A, B] = Await.result(eitherT.value, Duration(5, TimeUnit.SECONDS))
}

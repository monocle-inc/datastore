package com.monocle.datastore.util

import com.typesafe.config.ConfigFactory
import com.monocle.datastore.api.common.CommonResponse.RepoResponseF
import com.monocle.datastore.api.mongo.MongoRepo
import com.monocle.datastore.api.redis.RedisRepo
import com.monocle.datastore.api.sql.{SqlRepo, TableList}
import com.monocle.datastore.config.{MongoConfiguration, RedisConfiguration, SqlConfiguration}
import com.monocle.datastore.util.ArticleTableData.ArticleTable
import com.monocle.datastore.util.UserProfileTableData.UserProfileTable
import play.api.libs.json.Json
import reactivemongo.bson.Macros
import slick.jdbc.MySQLProfile.api._
import slick.jdbc.meta.MTable
import slick.lifted.{ProvenShape, Tag}

import scala.concurrent.{ExecutionContext, Future}

// Test models
case class UserProfile(id: String, createDate: Long, email: String, firstName: String, lastName: String)
case class Article(id: String, createDate: Long, userRef: String, content: String)

object UserProfileRepo {

  def findById(id: String)(implicit repo: SqlRepo[Tables], ec: ExecutionContext): RepoResponseF[Option[UserProfile]] = {
    repo.read { tables => tables.userTable.filter(_.id === id).result.headOption }
  }

  def saveUserProfile(profile: UserProfile)(implicit repo: SqlRepo[Tables], ec: ExecutionContext): RepoResponseF[Int] = {
    repo.write(_.userTable += profile)
  }
}

/******************************************************************************
 *  REDIS
 ******************************************************************************/
object RedisFixture {
  def createRedisConfiguration            = RedisConfiguration("db.redis", ConfigFactory.load)
  def redisDB(config: RedisConfiguration) = RedisDB(config)
}

object RedisFormats {
  implicit val userProfileJsonFormat = Json.format[UserProfile]
}

case class RedisDB(config: RedisConfiguration) extends RedisRepo

/******************************************************************************
 *  MONGO
 ******************************************************************************/
object BSONTestHandler {
  implicit val userProfileBSONHandler = Macros.handler[UserProfile]
  implicit val articalBSONHandler     = Macros.handler[Article]
}

object MongoFixture {
  def createMongoConfiguration(implicit ec: ExecutionContext)   = MongoConfiguration("db.mongo", ConfigFactory.load)
  def mongoCollection0(mongoConfiguration: MongoConfiguration)  = MongoCollection0(mongoConfiguration, "testCollection0")
  def mongoCollection1(mongoConfiguration: MongoConfiguration)  = MongoCollection1(mongoConfiguration, "testCollection1")
}

case class MongoCollection0(config: MongoConfiguration, collectionName: String) extends MongoRepo
case class MongoCollection1(config: MongoConfiguration, collectionName: String) extends MongoRepo

/******************************************************************************
 * SQL
 ******************************************************************************/
object SqlFixture {

  import ArticleTableData._
  import UserProfileTableData._

  private lazy val config           = ConfigFactory.load()
  private lazy val sqlConfiguration = SqlConfiguration("db.sql", config)
  private lazy val tables           = Tables(userProfileTableQuery, articleTableQuery)
  lazy val sqlDB                    = SqlDB(sqlConfiguration, tables)
}

case class Tables(userTable: TableQuery[UserProfileTable], articleTable: TableQuery[ArticleTable]) extends TableList

case class SqlDB(config: SqlConfiguration, tables: Tables) extends SqlRepo[Tables] {

  // Helper method
  def createTables(implicit ec: ExecutionContext): Future[List[Unit]] = {
    val t = List(tables.userTable, tables.articleTable)
    database.run(MTable.getTables).flatMap { currentTables =>
      val names = currentTables.map(_.name.name)
      val createUserProfileTableIfNotExist = t.filter(table => (!names.contains(table.baseTableRow.tableName))).map(_.schema.create)
      database.run(DBIO.sequence(createUserProfileTableIfNotExist))
    }
  }
}

object UserProfileTableData {
  class UserProfileTable(tag: Tag) extends Table[UserProfile](tag, "USER_PROFILE") {
    def id:           Rep[String]               = column[String]("name", O.PrimaryKey)
    def createDate:   Rep[Long]                 = column[Long]("createDate")
    def email:        Rep[String]               = column[String]("email")
    def firstName:    Rep[String]               = column[String]("firstName")
    def lastName:     Rep[String]               = column[String]("lastName")
    override def * :  ProvenShape[UserProfile]  = (id, createDate, email, firstName, lastName).mapTo[UserProfile]
  }

  lazy val userProfileTableQuery = TableQuery[UserProfileTable]
}

object ArticleTableData {
  class ArticleTable(tag: Tag) extends Table[Article](tag, "ARTICLE") {
    def id:           Rep[String]           = column[String]("id", O.PrimaryKey)
    def createDate:   Rep[Long]             = column[Long]("createDate")
    def userRef:      Rep[String]           = column[String]("userRef")
    def content:      Rep[String]           = column[String]("content")
    override def * :  ProvenShape[Article]  = (id, createDate, userRef, content).mapTo[Article]
  }

  lazy val articleTableQuery = TableQuery[ArticleTable]
}

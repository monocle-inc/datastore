package com.monocle.datastore

import java.util.UUID
import java.util.concurrent.TimeUnit

import cats.data.EitherT
import com.monocle.datastore.api.common.CommonResponse.RepoResponseF
import com.monocle.datastore.errors.RepoError
import com.monocle.datastore.util.BSONTestHandler._
import com.monocle.datastore.util._
import org.scalatest.{FlatSpec, Matchers}
import reactivemongo.bson.BSONDocument

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration.Duration
import scala.concurrent.{Await, Future}

class MongoDatastoreSpec extends FlatSpec with Matchers {

  private lazy val mongoConfig  = MongoFixture.createMongoConfiguration
  private lazy val collection0  = MongoFixture.mongoCollection0(mongoConfig)
  private lazy val collection1  = MongoFixture.mongoCollection1(mongoConfig)

  private def newUser(email: String, firstName: String, lastName: String): UserProfile = UserProfile(UUID.randomUUID().toString, System.currentTimeMillis, email, firstName, lastName)

  "User profile" should "insert" in {
    val userProfile = newUser("testUser@email.com", "Test", "User")

    exec(collection0.insert(userProfile))

    val mongoResponse = exec(collection0.readOne[UserProfile](BSONDocument("id" -> userProfile.id)))

    assert(mongoResponse.isRight)

    val foundProfile: Option[UserProfile] = mongoResponse.getOrElse(None)

    assert(foundProfile.isDefined)
    assert(foundProfile.get.id === userProfile.id)

    val deleteResult = exec(collection0.delete(BSONDocument("id" -> userProfile.id)))

    assert(deleteResult.isRight)

    val emptyProfileResult = exec(collection0.readOne[UserProfile](BSONDocument("id" -> userProfile.id)))

    assert(emptyProfileResult.isRight)
    assert(emptyProfileResult.getOrElse(None).isEmpty)
  }

  "Article for user" should "save" in {
    val userProfile = newUser("testUser@email.com", "Test", "User")
    val userArticle = Article(UUID.randomUUID().toString, System.currentTimeMillis, userProfile.id, "This is the article content!")

    exec(collection0.insert(userProfile))
    exec(collection1.insert(userArticle))

    val mongoResponse = exec(collection1.readOne[Article](BSONDocument("userRef" -> userProfile.id)))

    assert(mongoResponse.isRight)

    val foundArticle = mongoResponse.getOrElse(None)

    assert(foundArticle.isDefined)
    assert(foundArticle.get.userRef === userProfile.id)

    val deleteResult = exec(collection1.delete(BSONDocument("id" -> userArticle.id)))
    val deleteUserResult = exec(collection0.delete(BSONDocument("id" -> userProfile.id)))

    assert(deleteResult.isRight)
    assert(deleteUserResult.isRight)
  }

  "Mongo interpreter" should "interpret as expected" in {
    import cats.implicits._
    import com.monocle.datastore.api.mongo.MongoInterpreter._

    val user = newUser("test@email.com", "FirstName", "LastName")

    def program: MongoIOF[Option[UserProfile]] = {
      val bsonHandler = BSONTestHandler.userProfileBSONHandler
      val findQuery   = BSONDocument("id" -> user.id)
      val modifier    = BSONDocument("$set" -> BSONDocument("email" -> "newTest@email.com"))

      for {
        _             <- insert(user, bsonHandler)
        _             <- updateOne(findQuery, modifier)
        readResponse  <- readOne(findQuery, bsonHandler)
      } yield readResponse
    }

    // Executing our `program`
    val resultF: RepoResponseF[Option[UserProfile]] = program.foldMap(mongoInterpreter(collection0))

    val optUserE: Either[RepoError, Option[UserProfile]] = exec(resultF)

    assert(optUserE.isRight)

    val optUser: Option[UserProfile] = optUserE.getOrElse(None)

    assert(optUser.isDefined)

    val foundUser = optUser.get

    assert(foundUser.id == user.id)
    assert(foundUser.email != user.email)
  }

  private def exec[A, B](eitherT: EitherT[Future, A, B]): Either[A, B] = Await.result(eitherT.value, Duration(5, TimeUnit.SECONDS))
}

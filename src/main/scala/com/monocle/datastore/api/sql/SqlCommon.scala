package com.monocle.datastore.api.sql

import cats.InjectK
import cats.data.EitherT
import cats.free.Free
import com.monocle.datastore.api.common.CommonImplicitConversions
import com.monocle.datastore.api.common.CommonResponse.RepoResponseF
import com.monocle.datastore.api.sql.SqlCommon.{SqlReadAction, SqlResponse, SqlWriteAction}
import com.monocle.datastore.errors.RepoError
import slick.dbio.{Effect, NoStream, Streaming}
import slick.sql.SqlAction

import scala.concurrent.{ExecutionContext, Future}

object SqlCommon {

  type SqlResponse[T]         = Either[RepoError, T]
  type SqlReadAction[T]       = SqlAction[T, NoStream, Effect.Read]
  type SqlStreamReadAction[T] = SqlAction[T, Streaming[T], Effect.Read]
  type SqlWriteAction[T]      = SqlAction[T, NoStream, Effect.Write]
}

object SqlImplicitConversions extends CommonImplicitConversions {

  import com.monocle.datastore.errors.SqlErrorHandler._

  // Specializes on transforming a [[Future[A]]] to a [[SqlResponseF]]
  implicit def fromFutureA[A](future: Future[A])(implicit func: Future[SqlResponse[A]] => RepoResponseF[A], ec: ExecutionContext): RepoResponseF[A] = {
    func(future.map(Right(_)))
  }

  implicit def toSqlResponseF[A](future: Future[SqlResponse[A]])(implicit ec: ExecutionContext): RepoResponseF[A] = EitherT(future.recover(recoverPF))
}

sealed trait MySqlIO[A]

object MySqlOperations {
  case class MySqlReadOne[A, B <: TableList](query: B => SqlReadAction[A]) extends MySqlIO[A]
  case class MySqlWrite[A, B <: TableList](query: B => SqlWriteAction[A]) extends MySqlIO[Int]
}

class MySqlInjectedInteractions[F[_]](implicit I: InjectK[MySqlIO, F]) {

  import MySqlOperations._

  def writeToMySql[A, B <: TableList](sqlRepo: SqlRepo[B])(query: B => SqlWriteAction[A]): Free[F, Int] = {
    Free.inject[MySqlIO, F](MySqlWrite(query))
  }

  def readFromMySql[A, B <: TableList](sqlRepo: SqlRepo[B])(query: B => SqlReadAction[A]): Free[F, A] = {
    Free.inject[MySqlIO, F](MySqlReadOne(query))
  }
}

object MySqlInjectedInteractions {
  implicit def mySqlInjectedInteractions[F[_]](implicit I: InjectK[MySqlIO, F]): MySqlInjectedInteractions[F] = new MySqlInjectedInteractions[F]
}

object MySqlInterpreter {

  import MySqlOperations._
  import cats.free.Free.liftF
  import cats.~>

  type MySqlIOF[A] = Free[MySqlIO, A]

  def write[A, B <: TableList](sqlRepo: SqlRepo[B])(query: B => SqlWriteAction[A]): MySqlIOF[Int] = {
    liftF[MySqlIO, Int](MySqlWrite(query))
  }

  def read[A, B <: TableList](sqlRepo: SqlRepo[B])(query: B => SqlReadAction[A]): MySqlIOF[A] = {
    liftF[MySqlIO, A](MySqlReadOne(query))
  }

  def mySqlInterpreter[B <: TableList](sqlRepo: SqlRepo[B])(implicit ec: ExecutionContext): MySqlIO ~> RepoResponseF = new (MySqlIO ~> RepoResponseF) {
    override def apply[A](fa: MySqlIO[A]): RepoResponseF[A] = fa match {
      case MySqlWrite(query: (B => SqlWriteAction[A]) @unchecked)   => sqlRepo.write(query)(ec)
      case MySqlReadOne(query: (B => SqlReadAction[A]) @unchecked)  => sqlRepo.read(query)(ec)
    }
  }
}


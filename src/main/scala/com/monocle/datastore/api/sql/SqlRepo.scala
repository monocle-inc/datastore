package com.monocle.datastore.api.sql

import com.monocle.datastore.api.common.CommonResponse.RepoResponseF
import com.monocle.datastore.config.SqlConfiguration
import slick.dbio.{Effect, NoStream, Streaming}
import slick.jdbc.MySQLProfile
import slick.sql.SqlAction

import scala.concurrent.ExecutionContext

trait TableList

trait SqlRepo[A <: TableList] {

  import com.monocle.datastore.api.sql.SqlCommon._
  import com.monocle.datastore.api.sql.SqlImplicitConversions._

  val database: MySQLProfile.backend.Database = config.database

  protected val config: SqlConfiguration
  protected val tables: A

  def read[T](query: A => SqlReadAction[T])(implicit ec: ExecutionContext): RepoResponseF[T] = database.run(query(tables))
  def write[T](query: A => SqlWriteAction[T])(implicit ec: ExecutionContext): RepoResponseF[T] = database.run(query(tables))
}

object SqlRepo {
  type SqlReadAction[T]       = SqlAction[T, NoStream, Effect.Read]
  type SqlStreamReadAction[T] = SqlAction[T, Streaming[T], Effect.Read]
  type SqlWriteAction[T]      = SqlAction[T, NoStream, Effect.Write]
}


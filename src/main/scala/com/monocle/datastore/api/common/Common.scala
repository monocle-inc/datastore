package com.monocle.datastore.api.common

import cats.data.EitherT
import com.monocle.datastore.errors.RepoError
import simulacrum.typeclass

import scala.concurrent.Future
import scala.util.Try

object CommonResponse {

  type RepoResponseF[A] = EitherT[Future, RepoError, A]

  @typeclass
  trait Capture[M[_]] {
    def capture[A](a: => A): M[A]
  }

  implicit val tryCapture = new Capture[Try] {
    override def capture[A](a: => A): Try[A] = Try(a)
  }
}

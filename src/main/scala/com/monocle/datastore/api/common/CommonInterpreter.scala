package com.monocle.datastore.api.common

import cats.~>
import com.monocle.datastore.api.common.CommonResponse.RepoResponseF
import com.monocle.datastore.api.mongo.{MongoIO, MongoInterpreter, MongoRepo}
import com.monocle.datastore.api.redis.{RedisIO, RedisInterpreter, RedisRepo}
import com.monocle.datastore.api.sql._

import scala.concurrent.ExecutionContext

object CommonInterpreter {

  import cats.data.EitherK

  type MongoOrMySqlApplication[A]  = EitherK[MongoIO, MySqlIO, A]
  type DataStoreApplication[A]     = EitherK[RedisIO, MongoOrMySqlApplication, A]

  private def mongoOrMySqlInterpreter[A <: TableList](mongoRepo: MongoRepo, mySqlRepo: SqlRepo[A])(implicit ec: ExecutionContext): MongoOrMySqlApplication ~> RepoResponseF = {
    MongoInterpreter.mongoInterpreter(mongoRepo) or MySqlInterpreter.mySqlInterpreter(mySqlRepo)
  }

  def interpreter[A <: TableList](redisRepo: RedisRepo, mongoRepo: MongoRepo, sqlRepo: SqlRepo[A])(implicit ec: ExecutionContext): DataStoreApplication ~> RepoResponseF = {
    RedisInterpreter.redisInterpreter(redisRepo) or mongoOrMySqlInterpreter(mongoRepo, sqlRepo)
  }
}

package com.monocle.datastore.api.common

import cats.data.EitherT

import scala.concurrent.{ExecutionContext, Future}

trait CommonImplicitConversions {

  // Specialized on any [[Future[A]]]; converting [[A]] into a [[EitherT[Future, B, A]]]
  implicit def fromAnyFuture[A, B](future: Future[A])(implicit func: Future[A] => EitherT[Future, B, A], ec: ExecutionContext): EitherT[Future, B, A] = func(future)
}

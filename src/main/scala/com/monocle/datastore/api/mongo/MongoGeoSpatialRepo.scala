package com.monocle.datastore.api.mongo

import com.monocle.datastore.api.common.CommonResponse.RepoResponseF
import reactivemongo.api.collections.bson.BSONCollection
import reactivemongo.bson.{BSONDocument, BSONDocumentReader, array, document}

import scala.concurrent.ExecutionContext

case class GeoSpatialQuery(latitude:    Double,
                           longitude:   Double,
                           minDistance: Option[Long] = None,
                           maxDistance: Option[Long] = None,
                           limit:       Option[Long] = None)


trait MongoGeoSpatialRepo {

  import com.monocle.datastore.api.mongo.MongoImplicitConversions._

  def findNearest[T](geoQuery: GeoSpatialQuery)(implicit c: BSONCollection, ec: ExecutionContext, reader: BSONDocumentReader[T]): RepoResponseF[List[T]] = {
    findNearestInternal[T](geoQuery)
  }

  def findNearestWithQuery[T](geoQuery: GeoSpatialQuery)(query: () => BSONDocument)(implicit c: BSONCollection, ec: ExecutionContext, reader: BSONDocumentReader[T]): RepoResponseF[List[T]] = {
    findNearestInternal[T](geoQuery, Some(query()))
  }

  private def findNearestInternal[T](geoQuery: GeoSpatialQuery, query: Option[BSONDocument] = None)(implicit c: BSONCollection, ec: ExecutionContext, reader: BSONDocumentReader[T]): RepoResponseF[List[T]] = {
    import c.BatchCommands.AggregationFramework.GeoNear
    c.aggregatorContext[T](GeoNear(document(
      "type" -> "Point",
      "coordinates" -> array(geoQuery.latitude, geoQuery.longitude)
    ), distanceField = Some("distance.calculated"),
      minDistance = geoQuery.minDistance,
      maxDistance = geoQuery.maxDistance,
      query = query,
      limit = geoQuery.limit.getOrElse(25),
      spherical = true)).prepared.cursor.collect[List]().map(Right(_))
  }
}

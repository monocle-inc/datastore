package com.monocle.datastore.api.mongo

import reactivemongo.api.collections.bson.BSONCollection
import reactivemongo.api.{DefaultDB, ReadPreference}

import scala.concurrent.duration._
import scala.concurrent.{Await, ExecutionContext, Future}

case class MongoDatastore(server:               String,
                          db:                   String,
                          port:                 String,
                          connectTimeout:       Int,
                          authSource:           Option[String],
                          sslEnabled:           Boolean,
                          sslAllowInvalidCert:  Boolean,
                          tcpNoDelay:           Boolean,
                          keepAlive:            Boolean,
                          nbChannelsPerNode:    Int,
                          readPreference:       ReadPreference,
                          defaultDB:            Future[DefaultDB],
                          collectionName:       String)(implicit ec: ExecutionContext) {
  def collection: BSONCollection = Await.result(defaultDB.map(defDB => defDB(collectionName)), 10.seconds)
}

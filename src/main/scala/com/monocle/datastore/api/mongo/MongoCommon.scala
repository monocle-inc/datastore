package com.monocle.datastore.api.mongo

import cats.InjectK
import cats.data.EitherT
import cats.free.Free
import com.monocle.datastore.api.common.CommonImplicitConversions
import com.monocle.datastore.api.common.CommonResponse.RepoResponseF
import com.monocle.datastore.api.mongo.MongoCommon.MongoResponse
import com.monocle.datastore.errors.RepoError
import reactivemongo.api.commands.WriteResult
import reactivemongo.bson.{BSONDocument, BSONDocumentReader, BSONDocumentWriter}

import scala.concurrent.{ExecutionContext, Future}

object MongoCommon {

  type CollectionName     = String
  type MongoResponse[T]   = Either[RepoError, T]
//  type MongoResponseF[T]  = RepoResponseF[T, MongoError]
}

object MongoImplicitConversions extends CommonImplicitConversions {

  import com.monocle.datastore.errors.MongoErrorHandler._

  // Specializes on transforming a [[Future[A]]] to a [[MongoResponseF]]
  implicit def fromFutureA[A](future: Future[A])(implicit func: (Future[MongoResponse[A]]) => RepoResponseF[A], ec: ExecutionContext): RepoResponseF[A] = {
    func(future.map(Right(_)))
  }

  // Specializes on transforming a [[Future[WriteResult]]] to a [[MongoResponseF]]. This must react on the state of a [[WriteResult]]
  implicit def fromFutureWR(future: Future[WriteResult])(implicit func: (Future[MongoResponse[WriteResult]]) => RepoResponseF[WriteResult], ec: ExecutionContext): RepoResponseF[WriteResult] = {
    func(future.map(wr => if(wr.ok) Right(wr) else Left(RepoError("A mongo write result error occurred"))))
  }

  implicit def toMongoResponseF[T](future: Future[MongoResponse[T]])(implicit ec: ExecutionContext): RepoResponseF[T] = EitherT(future.recover(recoverPF))
}

sealed trait MongoIO[A]

object MongoOperations {
  case class MongoInsert[A](entity: A, writer: BSONDocumentWriter[A]) extends MongoIO[WriteResult]
  case class MongoUpsert[A](entity: A, query: BSONDocument, writer: BSONDocumentWriter[A]) extends MongoIO[WriteResult]
  case class MongoUpdateOne(query: BSONDocument, modifier: BSONDocument) extends MongoIO[WriteResult]
  case class MongoUpdateMany(query: BSONDocument, modifer: BSONDocument) extends MongoIO[WriteResult]
  case class MongoReadOne[A](query: BSONDocument, reader: BSONDocumentReader[A]) extends MongoIO[Option[A]]
  case class MongoReadMany[A](query: BSONDocument, reader: BSONDocumentReader[A], limit: Option[Int] = None, skip: Option[Int] = None) extends MongoIO[List[A]]
  case class MongoDelete(query: BSONDocument) extends MongoIO[WriteResult]
}

class MongoInjectedInteractions[F[_]](implicit I: InjectK[MongoIO, F]) {

  import MongoOperations._

  def insertInMongo[A](entity: A, writer: BSONDocumentWriter[A]): Free[F, WriteResult] = {
    Free.inject[MongoIO, F](MongoInsert(entity, writer))
  }

  def readOneFromMongo[A](query: BSONDocument, reader: BSONDocumentReader[A]): Free[F, Option[A]] = {
    Free.inject[MongoIO, F](MongoReadOne(query, reader))
  }
}

object MongoInjectedInteractions {
  implicit def mongoInjectedInteractions[F[_]](implicit I: InjectK[MongoIO, F]): MongoInjectedInteractions[F] = new MongoInjectedInteractions[F]
}

object MongoInterpreter {

  import MongoOperations._
  import cats.free.Free.liftF
  import cats.~>

  type MongoIOF[A] = Free[MongoIO, A]

  def insert[A](entity: A, writer: BSONDocumentWriter[A]): MongoIOF[WriteResult] = {
    liftF[MongoIO, WriteResult](MongoInsert(entity, writer))
  }

  def upsert[A](entity: A, query: BSONDocument, writer: BSONDocumentWriter[A]): MongoIOF[WriteResult] = {
    liftF[MongoIO, WriteResult](MongoUpsert(entity, query, writer))
  }

  def updateOne(query: BSONDocument, modifier: BSONDocument): MongoIOF[WriteResult] = {
    liftF[MongoIO, WriteResult](MongoUpdateOne(query, modifier))
  }

  def updateMany(query: BSONDocument, modifier: BSONDocument): MongoIOF[WriteResult] = {
    liftF[MongoIO, WriteResult](MongoUpdateMany(query, modifier))
  }

  def readOne[A](query: BSONDocument, reader: BSONDocumentReader[A]): MongoIOF[Option[A]] = {
    liftF[MongoIO, Option[A]](MongoReadOne(query, reader))
  }

  def readMany[A](query: BSONDocument, reader: BSONDocumentReader[A], limit: Option[Int] = None, skip: Option[Int] = None): MongoIOF[List[A]] = {
    liftF[MongoIO, List[A]](MongoReadMany(query, reader, limit, skip))
  }

  def delete(query: BSONDocument): MongoIOF[WriteResult] = {
    liftF[MongoIO, WriteResult](MongoDelete(query))
  }

  def mongoInterpreter(mongoRepo: MongoRepo)(implicit ec: ExecutionContext): MongoIO ~> RepoResponseF = new (MongoIO ~> RepoResponseF) {
    override def apply[A](fa: MongoIO[A]): RepoResponseF[A] = fa match {
      case MongoInsert(entity, writer)                => mongoRepo.insert(entity)(implicitly(writer), ec)
      case MongoUpsert(entity, query, writer)         => mongoRepo.upsert(entity)(query)(implicitly(writer), ec)
      case MongoUpdateOne(query, modifier)            => mongoRepo.updateOne(query)(modifier)(ec)
      case MongoUpdateMany(query, modifier)           => mongoRepo.updateMany(query)(modifier)(ec)
      case MongoReadOne(query, reader)                => mongoRepo.readOne(query)(implicitly(reader), ec)
      case MongoReadMany(query, reader, limit, skip)  => mongoRepo.readMany(limit, skip)(query)(implicitly(reader), ec)
      case MongoOperations.MongoDelete(query)         => mongoRepo.delete(query)(ec)
    }
  }
}

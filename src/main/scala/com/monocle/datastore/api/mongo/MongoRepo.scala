package com.monocle.datastore.api.mongo

import com.monocle.datastore.api.common.CommonResponse.RepoResponseF
import com.monocle.datastore.config.MongoConfiguration
import reactivemongo.api.QueryOpts
import reactivemongo.api.commands.WriteResult
import reactivemongo.bson.{BSONDocument, BSONDocumentReader, BSONDocumentWriter}

import scala.concurrent.ExecutionContext

trait MongoRepo {

  import com.monocle.datastore.api.mongo.MongoCommon._
  import com.monocle.datastore.api.mongo.MongoImplicitConversions._

  def config: MongoConfiguration
  def collectionName: CollectionName

  private lazy val collection = config.getMongoDatastore(collectionName).collection

  private def set[T](entity: T)(implicit writer: BSONDocumentWriter[T]): BSONDocument = BSONDocument("$set" -> entity)

  def insert[T](entity: T)(implicit writer: BSONDocumentWriter[T], ec: ExecutionContext): RepoResponseF[WriteResult] = {
    collection.insert(entity)
  }

  def upsert[T](entity: T)(query: => BSONDocument)(implicit writer: BSONDocumentWriter[T], ec: ExecutionContext): RepoResponseF[WriteResult] = {
    collection.update(query, set(entity), upsert = true, multi = false)
  }

  def updateOne(query: => BSONDocument)(modifier: => BSONDocument)(implicit ec: ExecutionContext): RepoResponseF[WriteResult] = {
    collection.update(false).one(query, modifier, false, false)
  }

  def updateMany(query: => BSONDocument)(modifier: BSONDocument)(implicit ec: ExecutionContext): RepoResponseF[WriteResult] = {
    collection.update(true).one(query, modifier, false, true)
  }

  def readOne[T](query: => BSONDocument)(implicit reader: BSONDocumentReader[T], ec: ExecutionContext): RepoResponseF[Option[T]] = {
    collection.find(query).one[T]
  }

  def readMany[T](limit: Option[Int] = None, skip: Option[Int] = None)(query: => BSONDocument)(implicit reader: BSONDocumentReader[T], ec: ExecutionContext): RepoResponseF[List[T]] = {
    collection.find(query).options(QueryOpts(skipN = skip.getOrElse(0))).cursor[T]().collect[List](limit.getOrElse(-1), true)
  }

  def delete(query: BSONDocument)(implicit ec: ExecutionContext): RepoResponseF[WriteResult] = {
    collection.delete().one(query)
  }
}
package com.monocle.datastore.api.redis

import cats.InjectK
import cats.free.Free
import com.monocle.datastore.api.common.CommonResponse.RepoResponseF
import play.api.libs.json.Format

import scala.concurrent.ExecutionContext

sealed trait RedisIO[A]

object RedisOperations {
  case class RedisRead[A](key: String, formatter: Format[A]) extends RedisIO[Option[A]]
  case class RedisWrite[A](key: String, entity: A, formatter: Format[A]) extends RedisIO[Boolean]
}

class RedisInjectedInteractions[F[_]](implicit I: InjectK[RedisIO, F]) {

  import RedisOperations._

  def writeToRedis[A](key: String, entity: A, formatter: Format[A]): Free[F, Boolean] = {
    Free.inject[RedisIO, F](RedisWrite(key, entity, formatter))
  }

  def readFromRedis[A](key: String, formatter: Format[A]): Free[F, Option[A]] = {
    Free.inject[RedisIO, F](RedisRead(key, formatter))
  }
}

object RedisInjectedInteractions {
  implicit def redisInjectedInteractions[F[_]](implicit I: InjectK[RedisIO, F]): RedisInjectedInteractions[F] = new RedisInjectedInteractions[F]
}

object RedisInterpreter {

  import RedisOperations._
  import cats.free.Free.liftF
  import cats.~>

  type RedisIOF[A] = Free[RedisIO, A]

  def set[A](key: String, entity: A, formatter: Format[A]): RedisIOF[Boolean] = {
    liftF[RedisIO, Boolean](RedisWrite(key, entity, formatter))
  }

  def get[A](key: String, formatter: Format[A]): RedisIOF[Option[A]] = {
    liftF[RedisIO, Option[A]](RedisRead(key, formatter))
  }

  def redisInterpreter(redisRepo: RedisRepo)(implicit ec: ExecutionContext): RedisIO ~> RepoResponseF = new (RedisIO ~> RepoResponseF) {
    override def apply[A](fa: RedisIO[A]): RepoResponseF[A] = fa match {
      case RedisRead(key, formatter)          => redisRepo.get(key)(implicitly(formatter))
      case RedisWrite(key, entity, formatter) => redisRepo.set(key, entity)(implicitly(formatter))
    }
  }
}

package com.monocle.datastore.api.redis

import cats.data.EitherT
import com.redis.serialization.Parse
import com.monocle.datastore.api.common.CommonResponse.RepoResponseF
import com.monocle.datastore.config.RedisConfiguration
import com.monocle.datastore.errors.RepoError
import play.api.libs.json.{Json, Format => JsonFormat}

import scala.concurrent.Future

trait RedisRepo {

  import Parse.Implicits._

  def config: RedisConfiguration

  private lazy val redisClient = config.getRedisClient

  def set[T](key: String, entity: T)(implicit format: JsonFormat[T]): RepoResponseF[Boolean] = {
    redisClient.set(key, Json.toJson(entity).toString())
  }

  def get[T](key: String)(implicit format: JsonFormat[T]): RepoResponseF[Option[T]] = {
    redisClient.get[String](key).flatMap(Json.parse(_).asOpt[T])
  }

  def delete(key: String): RepoResponseF[Option[Long]] = {
    redisClient.del(key)
  }

  private implicit def toEitherT[A](entity: A): EitherT[Future, RepoError, A] = {
    EitherT[Future, RepoError, A](Future.successful(Right(entity)))
  }
}

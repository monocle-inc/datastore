package com.monocle.datastore.api.dsl

import reactivemongo.bson.{BSONArray, BSONDocument, BSONDocumentWriter}

object MongoQueryLanguage {

  type MongoPath = String

  implicit class BSONLanguage(bson: BSONDocument) {
    def and(f: () => BSONDocument): BSONDocument = {
      BSONDocument("$and" -> BSONArray(bson, f()))
    }

    def and(otherBson: BSONDocument): BSONDocument = {
      BSONDocument("$and" -> BSONArray(bson, otherBson))
    }

    def or(f: () => BSONDocument): BSONDocument = {
      BSONDocument("$or" -> BSONArray(bson, f()))
    }

    def or(otherBson: BSONDocument): BSONDocument = {
      BSONDocument("$or" -> BSONArray(bson, otherBson))
    }
  }

  implicit class PathLanguage(path: MongoPath) {
    def is[T](entity: T)(implicit writer: BSONDocumentWriter[T]): BSONDocument = {
      BSONDocument(path -> entity)
    }

    def isNot[T](entity: T)(implicit writer: BSONDocumentWriter[T]): BSONDocument = {
      BSONDocument("$ne" -> BSONDocument(path -> entity))
    }
  }

}

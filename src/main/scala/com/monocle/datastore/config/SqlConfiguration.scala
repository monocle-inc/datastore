package com.monocle.datastore.config

import com.typesafe.config.Config
import slick.jdbc.MySQLProfile.api._

case class SqlConfiguration(path: String, configuration: Config) {
 lazy val database = Database.forConfig(path, configuration)
}
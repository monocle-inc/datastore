package com.monocle.datastore.config

import com.redis.RedisClient
import com.typesafe.config.Config

case class RedisConfiguration(path: String, config: Config) {

  val host  = config.getString(s"$path.host")
  val port  = config.getInt(s"$path.port")

  def getRedisClient = new RedisClient(host, port)
}

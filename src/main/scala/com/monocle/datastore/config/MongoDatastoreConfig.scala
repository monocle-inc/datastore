package com.monocle.datastore.config

import com.typesafe.config.Config
import com.monocle.datastore.api.mongo.MongoDatastore
import reactivemongo.api.{CrAuthentication, MongoConnectionOptions, MongoDriver, ReadPreference}

import scala.concurrent.ExecutionContext

case class MongoConfiguration (path: String, config: Config)(implicit ec: ExecutionContext) {
  val server              = config.getString(s"$path.host")
  val db                  = config.getString(s"$path.db")
  val port                = config.getString(s"$path.port")
  val connectTimeout      = config.getInt(s"$path.connectTimeout")
  val authSource          = config.getString(s"$path.authSource") match {
    case s if(s.length > 0) => Some(s)
    case _                  => None
  }
  val sslEnabled          = config.getBoolean(s"$path.sslEnabled")
  val sslAllowInvalidCert = config.getBoolean(s"$path.sslAllowInvalidCert")
  val tcpNoDelay          = config.getBoolean(s"$path.tcpNoDelay")
  val keepAlive           = config.getBoolean(s"$path.keepAlive")
  val nbChannelsPerNode   = config.getInt(s"$path.nbChannelsPerNode")
  val readPreference      = config.getString(s"$path.readPreference") match {
    case "primaryPreference"  => ReadPreference.primaryPreferred
    case "primary"            => ReadPreference.primary
    case "secondary"          => ReadPreference.secondary
    case "nearest"            => ReadPreference.nearest
    case _                    => ReadPreference.primary
  }

  val mongoOptions = MongoConnectionOptions(connectTimeout, authSource, sslEnabled, sslAllowInvalidCert,
    CrAuthentication, tcpNoDelay, keepAlive, nbChannelsPerNode
  )

  val mongoDriver     = new MongoDriver()
  val mongoConnection = mongoDriver.connection(List(s"$server:$port"))
  val defaultDB       = Some(mongoConnection.database(db))

  def getMongoDatastore(collection: String): MongoDatastore = {
    val collectionName = config.getString(s"db.mongo.collections.$collection") match {
      case s if(s.length > 0) => Some(s)
      case _                  => None
    }
    val mongoDatastore = for {
      collName      <- collectionName
      dbConnection  <- defaultDB
    } yield {
      MongoDatastore(server, db, port, connectTimeout, authSource, sslEnabled, sslAllowInvalidCert, tcpNoDelay,
        keepAlive, nbChannelsPerNode, readPreference, dbConnection, collName)
    }

    mongoDatastore match {
      case Some(dataStore)  => dataStore
      case None             => throw new Exception(s"Error creating MongoDataStore for collection $collection")
    }
  }
}
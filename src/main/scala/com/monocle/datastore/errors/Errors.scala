package com.monocle.datastore.errors

import com.monocle.datastore.api.mongo.MongoCommon.MongoResponse
import com.monocle.datastore.api.sql.SqlCommon.SqlResponse

//sealed trait RepoError {
//  def message: String
//}
case class RepoError(message: String)
//case class MongoError(message: String) extends RepoError
//case class SqlError(message: String) extends RepoError

object MongoErrorHandler {
  def recoverPF[T](): PartialFunction[Throwable, MongoResponse[T]] = {
    case t: Throwable => Left(RepoError(t.getMessage))
    case _            => Left(RepoError("Unknown mongo error occurred"))
  }
}

object SqlErrorHandler {
  def recoverPF[T](): PartialFunction[Throwable, SqlResponse[T]] = {
    case t: Throwable => Left(RepoError(t.getMessage))
    case _            => Left(RepoError("Unknown sql error occurred"))
  }
}
val projectName = "datastore"
val settings = Seq(
  organization  := "com.monocle",
  name          := projectName,
  version       := "0.3.0-SNAPSHOT",
  description   := "Database adapter library",
  scalaVersion  := "2.12.6",
  exportJars    := true,

  addCompilerPlugin("org.scalamacros" % "paradise" % "2.1.0" cross CrossVersion.full),

  scalacOptions += "-Ypartial-unification",

  resolvers ++= Resolver.jcenterRepo +: Resolver.bintrayRepo("scalaz", "releases") +: {
    //Add resolvers here
    Seq("typesafe" at "http://repo.typesafe.com/typesafe/releases/")
  },

  libraryDependencies ++= Seq(
    "org.reactivemongo"   %%  "reactivemongo"         % "0.15.0",
    "org.typelevel"       %%  "cats-core"             % "1.1.0",
    "org.typelevel"       %%  "cats-free"             % "1.1.0",
    "com.typesafe.slick"  %%  "slick"                 % "3.2.3",
    "mysql"               %   "mysql-connector-java"  % "5.1.41",
    "org.slf4j"           %   "slf4j-api"             % "1.7.25",
    "com.typesafe.slick"  %%  "slick-hikaricp"        % "3.2.3",
    "net.debasishg"       %%  "redisclient"           % "3.7",
    "io.argonaut"         %%  "argonaut-cats"         % "6.2.2",
    "com.typesafe.play"     %% "play-json"            % "2.6.7",
    "com.github.mpilquist"  %% "simulacrum"           % "0.13.0",
    "org.scalatest"       %%  "scalatest"             % "3.0.5"     % "test"
  ))

val testing = {
  val scalaTest = Tests.Argument(TestFrameworks.ScalaTest, "-W", "120", "60")
  val scalaCheck = Tests.Argument(TestFrameworks.ScalaCheck, "-maxSize", "500", "-minSuccessfulTests", "100", "-workers", "2", "-verbosity", "2")

  Seq(
    javaOptions += "-Dlogback.configurationFile=./test/resources/logback.xml",
    testOptions ++= Seq(scalaCheck, scalaTest)
  )
}

lazy val root = Project(id = projectName, base = file("."))
  .settings(settings)
  .settings(inConfig(Test)(javaOptions += "-Dconfig.file=conf/reference.test.conf"))
  .settings(inConfig(Compile)(inTask(doc)(sources := Seq.empty) ++
    inTask(packageDoc)(publishArtifact := false)))

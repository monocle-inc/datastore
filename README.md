# datastore

A database adapter library

**Supported DBs**
* MongoDB
* MySQL
* Redis

## MongoDB

### Configuration

```
db {
    mongo {
      host                = localhost
      port                = 27017
      db                  = mongo-test
      connectTimeout      = 5,
      authSource          = "",
      sslEnabled          = false,
      sslAllowInvalidCert = false,
      tcpNoDelay          = false,
      keepAlive           = false,
      nbChannelsPerNode   = 1
      readPreference      = "primary"
      collections {
        testCollection0 = testCollection0
      }
    }
}
```

### Creating the connection
1. Create a ```MongoConfiguration``` giving it a path to the configuration block and a ```typesafe.config.Config```
2. For each collection in the database, create a ```MongoRepo``` giving each a ```MongoConfiguration``` and a ```CollectionName```
3. Use a ```MongoRepo``` to execute your queries

```scala
import com.typesafe.config.ConfigFactory
import reactivemongo.api.commands.WriteResult
import reactivemongo.bson.{BSONDocument, BSONDocumentReader, BSONDocumentWriter}
import com.monocle.datastore.config.MongoConfiguration
import com.monocle.datastore.api.mongo.MongoCommon.MongoResponse
import com.monocle.datastore.api.mongo.MongoRepo

object MongoFixture {
  def createMongoConfiguration                      = MongoConfiguration("db.mongo", ConfigFactory.load)
  def mongoCollection0(config: MongoConfiguration)  = MongoCollection0(config, "testCollection0")
}

case class MongoCollection0(config: MongoConfiguration, collectionName: String) extends MongoRepo
```

###### Example

_```type MongoResponseF[T]  = EitherT[Future, MongoError, T]```_

```scala
val mongoConfig = MongoFixture.createMongoConfiguration
val collection0 = MongoFixture.mongoCollection0(mongoConfig)

val result: MongoResponseF[Option[User]] = collection0.readOne[User](BSONDocument("id" -> user.id))
```

## MySQL

### Configuration

```
db {
    sql {
      driver      = com.mysql.jdbc.Driver
      database    = sql-test
      url         = "jdbc:mysql://localhost:3306/sql-test?useSSL=false"
      user        = "root"
      password    = ""
      maxThreads  = 5
    }
}
```

### Creating the connection
1. Create a ```SqlConfiguration``` giving it a path to the configuration block and a ```typesafe.config.Config```
2. Extend ```SqlRepo``` using its read/write functions to execute queries.

```scala
import slick.lifted.Tag
import slick.jdbc.MySQLProfile.api._

import com.monocle.datastore.config.SqlConfiguration
import com.monocle.datastore.api.sql.SqlCommon.SqlResponseF

// Model
case class User(id: String, createDate: Long, email: String, firstName: String, lastName: String)

object UserTableData {
  // Table model
  class UserTable(tag: Tag) extends Table[User](tag, "USER") {
    def id          = column[String]("name", O.PrimaryKey)
    def createDate  = column[Long]("createDate")
    def email       = column[String]("email")
    def firstName   = column[String]("firstName")
    def lastName    = column[String]("lastName")
    override def *  = (id, createDate, email, firstName, lastName).mapTo[User]
  }

  // Used to build queries
  lazy val userTableQuery = TableQuery[UserTable]
}

// Captures the types of TableQuery available to a consumer
case class Tables(userTable: TableQuery[UserTable])

// Extending the SqlRepo trait provides default read/write functionality
case class SqlDB(config: SqlConfiguration, tables: Tables) extends SqlRepo[Tables]
```

###### Example

_```type SqlResponseF[T] = EitherT[Future, SqlError, T]```_

```scala
// Import [[userTableQuery]]
import UserTableData._

// Create configuration
val config  = new SqlConfiguration("db.sql", ConfigFactory.load)

// Define sql tables
val tables  = Tables(userTableQuery)

// Instantiate the database with the configuration and tables
val db      = new SqlDB(config, tables)

val user = User(UUID.randomUUID.toString, Date.now, "testuser@email.com", FirstName", "LastName")

val writeResult0: SqlResponseF[Int]          = db.write { tables => tables.userTable += user }
val readResult0: SqlResponseF[Option[User]]  = db.read { tables => tables.userTable.filter(_.id === "some-id").result.headOption }

```

Another pattern can be used to work with the database:

```scala
// Import [[userTableQuery]]
import UserTableData._

// Create configuration
val config = new SqlConfiguration("db.sql", ConfigFactory.load)

// Define sql tables
val tables = Tables(userTableQuery)

// Implicitly scoped database
implicit val db = new SqlDB(config, tables)


object UserProfileRepo {
  def findById(id: String)(implicit repo: SqlRepo[Tables], ec: ExecutionContext): SqlResponseF[Option[UserProfile]] = {
    repo.read { tables => tables.userTable.filter(_.id === id).result.headOption }
  }

  def saveUserProfile(profile: UserProfile)(implicit repo: SqlRepo[Tables], ec: ExecutionContext): SqlResponseF[Int] = {
    repo.write(_.userTable += profile)
  }
}

val user = User(UUID.randomUUID.toString, Date.now, "testuser@email.com", FirstName", "LastName")

val writeResult0: SqlResponseF[Int]         = UserProfileRepo.saveUserProfile(userProfile)
val readResult0: SqlResponseF[Option[User]] = UserProfileRepo.findById("some-id")

```

## Redis

### Configuration

```
db {
  redis {
    host  = "localhost"
    port  = 6379
  }
}
```

### Creating the connection

1. Create a ```RedisConfiguration``` giving it a path to the configuration block and a ```typesafe.config.Config```
2. Extend ```RedisRepo``` giving it a ```RedisConfiguration```

###### Notes
* Behind the scenes, ```RedisRepo``` serializes your types to a json string and then writes that value as an array of bytes to the redis client.
* Retrieving data from Redis has the opposite effect; a value is deserialized from an array of bytes, to a json string, and then finally inflated to your custom type.

```scala
object RedisFixture {
  def createRedisConfiguration            = RedisConfiguration("db.redis", ConfigFactory.load)
  def redisDB(config: RedisConfiguration) = RedisDB(config)
}

object RedisFormats {

  // Must be implicitly in scope to read and write custom types to Redis
  implicit val userProfileJsonFormat = Json.format[User]
}

case class RedisDB(config: RedisConfiguration) extends RedisRepo
```

###### Example

```scala
// Json serialization
import RedisFormats._

val redisConfig  = RedisFixture.createRedisConfiguration
val redisDB      = RedisFixture.redisDB(redisConfig)

val user = User(UUID.randomUUID.toString, Date.now, "testuser@email.com", FirstName", "LastName")

val writeResult0: Boolean     = redisDB.setBlocking(user.id, user)
val readResult0: Option[User] = redisDB.getBlocking[User]("some-id")
```
